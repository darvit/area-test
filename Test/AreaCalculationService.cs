﻿using System;

namespace Test
{
	public sealed class AreaCalculationService
	{
		public double Calculate(double a, double b, double c)
		{
			if (a <= 0)
			{
				throw new ArgumentOutOfRangeException("a");
			}
			if (b <= 0)
			{
				throw new ArgumentOutOfRangeException("b");
			}
			if (c <= 0)
			{
				throw new ArgumentOutOfRangeException("c");
			}

			bool exists = a + b >= c && b + c >= a && a + c >= b;
			if (!exists)
			{
				throw new Exception("Стороны не составляют треугольник");
			}

			double p = CalculateSemiperimeter(a, b, c);
			double s = Math.Sqrt(p * (p - a) * (p - b) * (p - c));

			return s;
		}

		private double CalculateSemiperimeter(double a, double b, double c)
		{
			return (a + b + c) / 2;
		}
	}
}