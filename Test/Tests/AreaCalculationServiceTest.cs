﻿using System;

using Xunit;

namespace Test.Tests
{
	public sealed class AreaCalculationServiceTest
	{
		private AreaCalculationService m_Service;

		[Fact]
		public void CalculateAreaPass()
		{
			Init();
			Assert.Equal(6, m_Service.Calculate(3, 4, 5));
		}

		[Fact]
		public void CalculateAreaWrong()
		{
			Init();
			Assert.NotEqual(7, m_Service.Calculate(3, 4, 5));
		}

		[Theory]
		[InlineData(4, 6, 7)]
		[InlineData(8, 10, 14)]
		[InlineData(24, 16, 37)]
		public void CalculatePass(int a, int b, int c)
		{
			Init();
			double area = m_Service.Calculate(a, b, c);
			Assert.True(area > 0);
		}

		[Theory]
		[InlineData(3, 4, 8)]
		[InlineData(5, 15, 9)]
		[InlineData(50, 1, 9)]
		public void NotTriangleTheory(int a, int b, int c)
		{
			Init();
			Assert.Throws(typeof(Exception), () => m_Service.Calculate(a, b, c));
		}

		private void Init()
		{
			m_Service = new AreaCalculationService();
		}
	}
}